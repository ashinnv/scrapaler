
import re
import nltk

from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

testdat = ["jabber is an app that reads minds", "combo is a program the paints trees", "I made a prgram that eats banannas. It's called jumbo.", "Ted Taylor is the only person who ever lit a cigarette with a nuclear weapon's explosion.","Dante is a program that builds houses out of coke bottles","BILLY IS A BOY WHO EATS BANANNAS"]
reList = ['is a|an * that *']
nouns = []

#pullReg pulls the regular expression match out of a line of text.
def pullReg(line):
  for r in reList:
    reg = re.compile(r)
    if reg.search(line.lower()):
      #print("======Won:",line)
      continue
    else:
      #print("Failed====:", line)
      continue
      
if __name__ == "__main__":
  stemmer = PorterStemmer()

  for line in testdat:
    pullReg(line)
    oput = nltk.word_tokenize(line) 
    stwd = [stemmer.stem(oput) for oput in oput]
    stwd_stemmed = nltk.pos_tag(stwd)
    for word in stwd_stemmed:
      if word[1] == "NN":
        nouns.append(word[0])
      #print(word[1])
    for word in nouns:
      print(word)  
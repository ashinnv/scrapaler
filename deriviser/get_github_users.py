import requests
from pprint import pprint

# https://raw.githubusercontent.com/dozerman-ms/glapher/main/README.md
class UrlObj:  
  
  def __init__(self, input_string): #https://github.com/dozerman-ms/glapher
    spl = input_string.split('/')# ['https:', '', 'github.com', 'dozerman-ms', 'glapher']
    if len(spl) < 3:
      print("LESSSSS")
    #print("SPL:::::", spl)
    
    self.lead = "https://raw.githubusercontent.com"
    self.user = spl[3]
    self.proj = spl[4]
    
    self.brch = "main"
    self.targ_file = "README.md"
    
  def dump_proj(self):
    ret = self.lead + "/" +self.user+"/"+self.proj+"/main/"+"README.md"
    return ret

# https://github.com/dozerman-ms/cantale 
def get_user_data(uname):
  project_objects = []
  print(uname)
  
  #username = "dozerman-ms"
  url = f"https://api.github.com/users/{uname}"
  user_data = requests.get(url).json()

  repos = user_data["repos_url"]
  projects = requests.get(repos).json()

  
  for project in projects:
    print("Project:", project)
    uo = UrlObj(project["html_url"])
    project_objects.append(uo)
    #print(project["html_url"])
    for ob in project_objects:
      print(ob.dump_proj())
    
  
if __name__=="__main__":
  get_user_data("dozerman-ms")

